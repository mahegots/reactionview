package com.upax.reactionview;

import android.content.res.Resources;

public class Util {
    public static int DIVIDE = Util.dpToPx(5);

    public static int HEIGHT_VIEW_REACTION = Util.dpToPx(250);

    public static int WIDHT_VIEW_REACTION = Util.dpToPx(300);

    public static final int MAX_ALPHA = 255;

    public static final int MIN_ALPHA = 150;

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
}
