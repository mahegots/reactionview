package com.upax.reactionview;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.collection.ArraySet;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button btnLike;
    ReactionView reactionView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        btnLike = findViewById(R.id.btn_like);
        reactionView = findViewById(R.id.view_reaction);
        reactionView.setVisibility(View.GONE);


        ArrayList<Emotion> emotions=new ArrayList<>();
        emotions.add(new Emotion(this, "Like", R.drawable.like));
        emotions.add(new Emotion(this, "Love", R.drawable.love));
        emotions.add(new Emotion(this, "Haha", R.drawable.haha));
        emotions.add(new Emotion(this, "Wow", R.drawable.wow));
        emotions.add(new Emotion(this, "Cry", R.drawable.cry));
        emotions.add(new Emotion(this, "Angry", R.drawable.angry));
        emotions.add(new Emotion(this, "Like2", R.drawable.like));
        emotions.add(new Emotion(this, "Love2", R.drawable.love));
        
        
        reactionView.addEmotions(emotions);
        reactionView.addSelectionListener(emotion -> {
            Log.e("Error", emotion.title);
            reactionView.setVisibility(View.GONE);
        });
        btnLike.setOnClickListener(view -> reactionView.show());
    }


}
