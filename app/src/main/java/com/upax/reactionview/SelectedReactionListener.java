package com.upax.reactionview;



interface SelectedReactionListener {
    void onSelected(Emotion emotion);
    }