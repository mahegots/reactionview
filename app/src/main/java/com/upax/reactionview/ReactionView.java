package com.upax.reactionview;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import java.util.ArrayList;

import static com.upax.reactionview.Util.DIVIDE;
import static com.upax.reactionview.Util.HEIGHT_VIEW_REACTION;


public class ReactionView extends View {

    public static final long DURATION_ANIMATION = 200;
    public static final long DURATION_BEGINNING_EACH_ITEM = 300;
    public static final long DURATION_BEGINNING_ANIMATION = 900;
    private SelectedReactionListener selectedReactionListener;
    private EaseOutBack easeOutBack;
    private Board board;
    private ArrayList<Emotion> emotions = new ArrayList<>();
    private StateDraw state = StateDraw.BEGIN;
    private int currentPosition = 0;

    public ReactionView(Context context) {
        super(context);
        init();
    }

    public ReactionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ReactionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void addSelectionListener(SelectedReactionListener selectedReactionListener) {
        this.selectedReactionListener = selectedReactionListener;
    }

    public void addEmotions(ArrayList<Emotion> emotions) {
        this.emotions = emotions;
        board = new Board(getContext(), emotions.size());
        setLayerType(LAYER_TYPE_SOFTWARE, board.boardPaint);
        initElement();
    }

    private void init() {
        board = new Board(getContext(), emotions.size());
        setLayerType(LAYER_TYPE_SOFTWARE, board.boardPaint);
        initElement();
    }

    private void initElement() {
        board.currentY = HEIGHT_VIEW_REACTION + 10;
        for (Emotion e : emotions) {
            e.currentY = board.currentY + DIVIDE;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (state != null) {
            board.drawBoard(canvas);
            for (Emotion emotion : emotions) {
                emotion.drawEmotion(canvas);
            }
        }
    }

    private void beforeAnimateBeginning() {
        board.beginHeight = Board.BOARD_HEIGHT_NORMAL;
        board.endHeight = Board.BOARD_HEIGHT_NORMAL;

        board.beginY = Board.BOARD_BOTTOM + 150;
        board.endY = Board.BOARD_Y;

        easeOutBack = EaseOutBack.newInstance(DURATION_BEGINNING_EACH_ITEM, Math.abs(board.beginY - board.endY), 0);

        for (int i = 0; i < emotions.size(); i++) {
            emotions.get(i).endY = Board.BASE_LINE - Emotion.NORMAL_SIZE;
            emotions.get(i).beginY = Board.BOARD_BOTTOM + 150;
            emotions.get(i).currentX = i == 0 ? Board.BOARD_X + DIVIDE : emotions.get(i - 1).currentX + emotions.get(i - 1).currentSize + DIVIDE;
        }
    }

    private void beforeAnimateChoosing() {
        board.beginHeight = board.getCurrentHeight();
        board.endHeight = Board.BOARD_HEIGHT_MINIMAL;

        for (int i = 0; i < emotions.size(); i++) {
            emotions.get(i).beginSize = emotions.get(i).currentSize;

            if (i == currentPosition) {
                emotions.get(i).endSize = Emotion.CHOOSE_SIZE;
            } else {
                emotions.get(i).endSize = Emotion.MINIMAL_SIZE;
            }
        }
    }

    private void beforeAnimateNormalBack() {
        board.beginHeight = board.getCurrentHeight();
        board.endHeight = Board.BOARD_HEIGHT_NORMAL;

        for (Emotion emotion : emotions) {
            emotion.beginSize = emotion.currentSize;
            emotion.endSize = Emotion.NORMAL_SIZE;
        }
    }

    private void calculateInSessionChoosingAndEnding(float interpolatedTime) {
        board.setCurrentHeight(board.beginHeight + (int) (interpolatedTime * (board.endHeight - board.beginHeight)));

        for (int i = 0; i < emotions.size(); i++) {
            emotions.get(i).currentSize = calculateSize(i, interpolatedTime);
            emotions.get(i).currentY = Board.BASE_LINE - emotions.get(i).currentSize;
        }
        calculateCoordinateX();
        invalidate();
    }

    private void calculateInSessionBeginning(float interpolatedTime) {
        float currentTime = interpolatedTime * DURATION_BEGINNING_ANIMATION;

        if (currentTime > 0) {
            board.currentY = board.endY + easeOutBack.getCoordinateYFromTime(Math.min(currentTime, DURATION_BEGINNING_EACH_ITEM));
        }

        for (int i = 0; i < emotions.size(); i++) {
            if (currentTime >= (100 * (i + 1))) {
                emotions.get(i).currentY = emotions.get(i).endY + easeOutBack.getCoordinateYFromTime(Math.min(currentTime - 100, DURATION_BEGINNING_EACH_ITEM));
            }
        }

        invalidate();
    }

    private int calculateSize(int position, float interpolatedTime) {
        int changeSize = emotions.get(position).endSize - emotions.get(position).beginSize;
        return emotions.get(position).beginSize + (int) (interpolatedTime * changeSize);
    }

    private void calculateCoordinateX() {
        emotions.get(0).currentX = Board.BOARD_X + DIVIDE;
        emotions.get(emotions.size() - 1).currentX = Board.BOARD_X + Board.BOARD_WIDTH - DIVIDE - emotions.get(emotions.size() - 1).currentSize;

        for (int i = 1; i < currentPosition; i++) {
            emotions.get(i).currentX = emotions.get(i - 1).currentX + emotions.get(i - 1).currentSize + DIVIDE;
        }

        for (int i = emotions.size() - 2; i > currentPosition; i--) {
            emotions.get(i).currentX = emotions.get(i + 1).currentX - emotions.get(i).currentSize - DIVIDE;
        }

        if (currentPosition != 0 && currentPosition != emotions.size() - 1) {
            if (currentPosition <= (emotions.size() / 2 - 1)) {
                emotions.get(currentPosition).currentX = emotions.get(currentPosition - 1).currentX + emotions.get(currentPosition - 1).currentSize + DIVIDE;
            } else {
                emotions.get(currentPosition).currentX = emotions.get(currentPosition + 1).currentX - emotions.get(currentPosition).currentSize - DIVIDE;
            }
        }
    }

    public void show() {
        state = StateDraw.BEGIN;
        setVisibility(VISIBLE);
        beforeAnimateBeginning();
        startAnimation(new BeginningAnimation());
    }

    private void selected(int position) {
        if (currentPosition == position && state == StateDraw.CHOOSING) {
            return;
        }
        state = StateDraw.CHOOSING;
        currentPosition = position;
        startAnimation(new ChooseEmotionAnimation());
    }

    public void backToNormal() {
        state = StateDraw.NORMAL;
        selectedReactionListener.onSelected(emotions.get(currentPosition));
        startAnimation(new ChooseEmotionAnimation());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean handled = false;
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                handled = true;
                break;
            case MotionEvent.ACTION_MOVE:
                for (int i = 0; i < emotions.size(); i++) {
                    if (event.getX() > emotions.get(i).currentX && event.getX() < emotions.get(i).currentX + emotions.get(i).currentSize) {
                        selected(i);
                        break;
                    }
                }
                handled = true;
                break;
            case MotionEvent.ACTION_UP:
                backToNormal();

                handled = true;
                break;
        }
        return handled;
    }

    enum StateDraw {
        BEGIN,
        CHOOSING,
        NORMAL
    }

    class ChooseEmotionAnimation extends Animation {
        public ChooseEmotionAnimation() {
            if (state == StateDraw.CHOOSING) {
                beforeAnimateChoosing();
            } else if (state == StateDraw.NORMAL) {
                beforeAnimateNormalBack();
            }
            setDuration(DURATION_ANIMATION);
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            calculateInSessionChoosingAndEnding(interpolatedTime);
        }
    }

    class BeginningAnimation extends Animation {

        public BeginningAnimation() {
            beforeAnimateBeginning();
            setDuration(DURATION_BEGINNING_ANIMATION);
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            calculateInSessionBeginning(interpolatedTime);
        }
    }
}
